package ru.utraders.test;

public interface RateLimiter {

    void resetLimiterIfNeeded();

    boolean isRequestPermitted();

    void registerRequest();
}
