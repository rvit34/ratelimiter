package ru.utraders.test;


public class ServiceApi {

    private final RateLimiter rateLimiter;

    public ServiceApi(RateLimiter rateLimiter) {
        this.rateLimiter = rateLimiter;
    }

    public String getGreeting() {

        rateLimiter.resetLimiterIfNeeded();
        if (!rateLimiter.isRequestPermitted()){
            return "";
        }

        rateLimiter.registerRequest();
        return "Hello World";
    }
}
