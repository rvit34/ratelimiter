package ru.utraders.test;

import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

public class RecentTimeRangeRateLimiter implements RateLimiter {

    private final Clock clock;

    private final long limitRequestsCount;
    private final long limitTimeInMillis;

    private final NavigableMap<Long, Integer> requests; // key == timestamp, value == count of requests

    public RecentTimeRangeRateLimiter(Clock clock, long limitRequestsCount, long limitTimeInMillis) {
        this.clock = clock;
        this.limitRequestsCount = limitRequestsCount;
        this.limitTimeInMillis = limitTimeInMillis;
        this.requests = new ConcurrentSkipListMap<>();
    }

    @Override
    public void resetLimiterIfNeeded() {
        clearRequests();
    }

    @Override
    public boolean isRequestPermitted() {

        final long endInt = clock.getCurrentTime();
        final long startInt = endInt - limitTimeInMillis;

        int countOfGrantedRequestsInRange = requests
                .subMap(startInt, true, endInt, true)
                .entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .reduce((e1, e2) -> e1 + e2)
                .orElse(0);

        return countOfGrantedRequestsInRange < limitRequestsCount;
    }

    @Override
    public void registerRequest() {
        requests.merge(clock.getCurrentTime(), 1, (a, b) -> a + b);
    }

    private void clearRequests() {
        final long startInt = 0L;
        final long endInt = clock.getCurrentTime() - limitTimeInMillis - 1;
        if (endInt > startInt) {
            requests.subMap(0L, endInt).forEach((ts, count) -> requests.remove(ts));
        }
    }
}
