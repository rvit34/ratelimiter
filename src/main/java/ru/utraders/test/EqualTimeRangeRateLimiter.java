package ru.utraders.test;

public class EqualTimeRangeRateLimiter implements RateLimiter {

    private long requestsCount;
    private long lastTimestamp;
    private long resetsCount = 1;

    private final Clock clock;

    private final long limitRequestsCount;
    private final long limitTimeInMillis;

    public EqualTimeRangeRateLimiter(Clock clock, long limitRequestsCount, long limitTimeInMillis) {
        this.limitRequestsCount = limitRequestsCount;
        this.limitTimeInMillis = limitTimeInMillis;
        this.clock = clock;
    }

    @Override
    public void resetLimiterIfNeeded() {
        if (clock.getCurrentTime() > (limitTimeInMillis*resetsCount) && requestsCount + 1 > limitRequestsCount) {
            requestsCount = 0;
            resetsCount++;
        }
    }

    @Override
    public boolean isRequestPermitted() {
        long tsDiff = clock.getCurrentTime() - lastTimestamp;
        return !(tsDiff < limitTimeInMillis && requestsCount + 1 > limitRequestsCount);
    }

    @Override
    public void registerRequest() {
        this.lastTimestamp = clock.getCurrentTime();
        requestsCount++;
    }

}