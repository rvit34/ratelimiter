package ru.utraders.test;

public interface Clock {

    long getCurrentTime();

    void tick(long timeInMillis);

}
