package ru.utraders.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class RecentTimeRangeRateLimiterTest {

    protected RateLimiter rateLimiter;
    protected ServiceApi api;
    protected Clock clock;

    @Before
    public void setUp() {
        clock = new TestClock();
        rateLimiter = new RecentTimeRangeRateLimiter(clock, 2, 1000);
        api = new ServiceApi(rateLimiter);
    }

    @Test
    public void testLimiter() {
        Assert.assertTrue(accessGranted());
        clock.tick(400);  // 0.4s
        Assert.assertTrue(accessGranted());
        clock.tick(400); // 0.8s
        Assert.assertTrue(accessDenied());
        Assert.assertTrue(accessDenied());
        Assert.assertTrue(accessDenied());
        clock.tick(400); //1.2s
        Assert.assertTrue(accessGranted()); //0.2-1.2 (1 granted request exists at 0.4s, so it should be granted)
        clock.tick(400); //1.6s
        Assert.assertTrue(accessGranted()); //0.6-1.6 (1 granted request exists at 1.2s, so it should be granted)
        clock.tick(200); //1.8s
        Assert.assertTrue(accessDenied());
        Assert.assertTrue(accessDenied());
        Assert.assertTrue(accessDenied());
        clock.tick(200); //2s
        Assert.assertTrue(accessDenied());//1.0-2.0 (2 granted request exist at 1.2s and 1.6s, so it should be denied)
        clock.tick(200); //2.2s
        Assert.assertTrue(accessDenied()); //1.2-2.2 (2 granted request exist at 1.2s and 1.6s, so it should be denied)
        clock.tick(100); //2.3s
        Assert.assertTrue(accessGranted()); //1.3-2.3 (1 granted request exists at 1.6, so it should be granted)
        clock.tick(1100); //3.4s
        Assert.assertTrue(accessGranted());// 2.4-3.4 (no granted request exists, so it should be granted)
        Assert.assertTrue(accessGranted());// 2.4-3.4 (1 granted request exists at 3.4, so it should be granted)
        Assert.assertTrue(accessDenied());
    }

    protected boolean accessGranted() {
        return "Hello World".equals(api.getGreeting());
    }

    protected boolean accessDenied() {
        return "".equals(api.getGreeting());
    }

    protected class TestClock implements Clock {

        private long time;

        @Override
        public long getCurrentTime() {
            return time;
        }
        @Override
        public void tick(long timeInMillis) {
            time +=timeInMillis;
        }
    }
}