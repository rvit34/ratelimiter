package ru.utraders.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EqualTimeRangeRateLimiterTest extends RecentTimeRangeRateLimiterTest {

    @Before
    public void setUp() {
        clock = new TestClock();
        rateLimiter = new EqualTimeRangeRateLimiter(clock, 2, 1000);
        api = new ServiceApi(rateLimiter);
    }

    @Test
    @Override
    public void testLimiter() {

        Assert.assertTrue(accessGranted());
        clock.tick(400);
        Assert.assertTrue(accessGranted());
        clock.tick(400); //800
        Assert.assertTrue(accessDenied());
        Assert.assertTrue(accessDenied());
        Assert.assertTrue(accessDenied());

        clock.tick(400); //1200
        Assert.assertTrue(accessGranted());
        clock.tick(400); //1600
        Assert.assertTrue(accessGranted());
        clock.tick(200); //1800
        Assert.assertTrue(accessDenied());
        Assert.assertTrue(accessDenied());
        Assert.assertTrue(accessDenied());
        clock.tick(200); //2000
        Assert.assertTrue(accessDenied());
        clock.tick(1); //2001
        Assert.assertTrue(accessGranted());
        Assert.assertTrue(accessGranted());
        Assert.assertTrue(accessDenied());

    }
}
